import * as React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./Navbar";
import User from "./User";
import UserCreate from "./UserCreate";
import UserUpdate from "./UserUpdate";

export default function App() {
  return (
    <Router>
      <Navbar />
      <div>
        <Switch>
          <Route path="/update/:id">
            <UserUpdate />
          </Route>
          <Route path="/create">
            <UserCreate />
          </Route>

          <Route path="/">
            <User />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
