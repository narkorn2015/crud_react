import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
// table
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
//Avatars
import Avatar from "@mui/material/Avatar";
import { Link } from "react-router-dom";
//button
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    marginTop: theme.spacing(2),
  },
  navlink: {
    color: "white",
    textDecoration: "none",
  },
}));

// ดึงข้อมูล
export default function SimplePaper() {
  const classes = useStyles();
  const [user, setUser] = useState([]);
  useEffect(() => {
    UserRead();
  });

  // Read data
  const UserRead = () => {
    // alert("test");
    fetch("https://www.mecallapi.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        setUser(result);
      });
  };

  // Update
  const UpdateUser = (id) => {
    window.location = "/update/" + id;
  };
  // Action Delete

  const UserDelete = (id) => {
    var data = {
      id: id,
    };
    fetch("https://www.mecallapi.com/api/users/delete", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          UserRead();
        }
      });
  };

  return (
    <>
      <div className={classes.root}>
        <Container className={classes.container} maxWidth="lg">
          <Paper className={classes.paper}>
            <Box sx={{ display: "flex", p: 1, bgcolor: "background.paper" }}>
              <Box flexGrow={1}>
                <Typography variant="h6" gutterBottom component="div">
                  ตารางแสดงยูส
                </Typography>
              </Box>
              <Box>
                <Link className={classes.navlink} to="/create">
                  <Button variant="contained">Create</Button>
                </Link>
              </Box>
            </Box>

            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">id</TableCell>
                    <TableCell align="left">Pic</TableCell>
                    <TableCell align="left">fname(ชื่อ)</TableCell>
                    <TableCell align="left">lname&nbsp;(นามสกุล)</TableCell>
                    <TableCell align="left">Email Address&nbsp;</TableCell>
                    <TableCell align="left">URL Image</TableCell>
                    <TableCell align="left">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {user.map((user) => (
                    <TableRow
                      key={user.name}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row" align="center">
                        {user.id}
                      </TableCell>
                      <TableCell>
                        <Box>
                          <Avatar
                            align="right"
                            // alt="Remy Sharp"
                            src={user.avatar}
                          />
                        </Box>
                      </TableCell>
                      <TableCell align="left">{user.fname}</TableCell>
                      <TableCell align="left">{user.lname}</TableCell>
                      <TableCell align="left">{user.username}</TableCell>
                      <TableCell align="left">{user.avatar}</TableCell>
                      <TableCell align="left">
                        <ButtonGroup
                          color="primary"
                          aria-label="outlined primary button group"
                        >
                          <Button onClick={() => UpdateUser(user.id)}>
                            Edit
                          </Button>
                          <Button onClick={() => UserDelete(user.id)}>
                            Del
                          </Button>
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </div>
    </>
  );
}
